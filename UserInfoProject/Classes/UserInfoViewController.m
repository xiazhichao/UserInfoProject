//
//  UserInfoViewController.m
//  UserInfoProject
//
//  Created by xzc on 2018/9/4.
//

#import "UserInfoViewController.h"
//#import <AFNetworking/AFNetworking.h>
#import "SecUserInfoViewController.h"
#define JCYCharityBundle [NSBundle bundleWithURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"Resurce" withExtension:@"bundle"]]
@interface UserInfoViewController ()

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    self.view.backgroundColor = [UIColor whiteColor];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SecUserInfoViewController *secVc = [[SecUserInfoViewController alloc]initWithNibName:@"SecUserInfoViewController" bundle:bundle];
        //        Class VC = NSClassFromString(@"UserInfoViewController");
        //
        //        UIViewController *vc = [[VC alloc]init];
                [self.navigationController pushViewController:secVc animated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

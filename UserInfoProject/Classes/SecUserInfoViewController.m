//
//  SecUserInfoViewController.m
//  UserInfoProject
//
//  Created by xzc on 2018/9/13.
//

#import "SecUserInfoViewController.h"
@interface SecUserInfoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation SecUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    [self.imageView setImage:[UIImage imageNamed:@"ask_answer" inBundle:bundle compatibleWithTraitCollection:nil]];
//     [btDel setImage:[UIImage imageNamed:@"chi_delpic" inBundle:JCYCharityBundle compatibleWithTraitCollection:nil] forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

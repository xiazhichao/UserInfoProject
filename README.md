# UserInfoProject

[![CI Status](https://img.shields.io/travis/“xzc”/UserInfoProject.svg?style=flat)](https://travis-ci.org/“xzc”/UserInfoProject)
[![Version](https://img.shields.io/cocoapods/v/UserInfoProject.svg?style=flat)](https://cocoapods.org/pods/UserInfoProject)
[![License](https://img.shields.io/cocoapods/l/UserInfoProject.svg?style=flat)](https://cocoapods.org/pods/UserInfoProject)
[![Platform](https://img.shields.io/cocoapods/p/UserInfoProject.svg?style=flat)](https://cocoapods.org/pods/UserInfoProject)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UserInfoProject is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'UserInfoProject'
```

## Author

“xzc”, “1256511693@qq.com”

## License

UserInfoProject is available under the MIT license. See the LICENSE file for more info.

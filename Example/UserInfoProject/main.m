//
//  main.m
//  UserInfoProject
//
//  Created by “xzc” on 09/04/2018.
//  Copyright (c) 2018 “xzc”. All rights reserved.
//

@import UIKit;
#import "UserInfoProjectAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UserInfoProjectAppDelegate class]));
    }
}

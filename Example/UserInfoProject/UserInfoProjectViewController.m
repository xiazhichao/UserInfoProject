//
//  UserInfoProjectViewController.m
//  UserInfoProject
//
//  Created by “xzc” on 09/04/2018.
//  Copyright (c) 2018 “xzc”. All rights reserved.
//

#import "UserInfoProjectViewController.h"

@interface UserInfoProjectViewController ()

@end

@implementation UserInfoProjectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        Class VC = NSClassFromString(@"UserInfoViewController");
        
        UIViewController *vc = [[VC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    });

    
	// Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

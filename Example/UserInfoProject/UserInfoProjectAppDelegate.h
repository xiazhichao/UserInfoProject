//
//  UserInfoProjectAppDelegate.h
//  UserInfoProject
//
//  Created by “xzc” on 09/04/2018.
//  Copyright (c) 2018 “xzc”. All rights reserved.
//

@import UIKit;

@interface UserInfoProjectAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
